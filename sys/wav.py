#!/usr/bin/env python3

#
#   generate the values for a wave
#

import math
import sys

TAU = math.pi * 2.0
adder = 440

def sin_wave(freq, time):
    return adder + adder * (math.sin(time * freq * TAU))

def square_wave(freq, time, peak):
    return peak if TAU * freq * time > 0.5 else (-peak)

def main():
    if len(sys.argv) < 2:
        raise ValueError("Please enter a wave type to generate!")

    s = []
    wave = sys.argv[1]
    
    # sine
    for t in range(0, 100):
        if wave == "sine":
            s.append(str(int(sin_wave(0.06, t))))
        elif wave == "sqre":
            s.append(str(int(square_wave(0.1, t, 127))))
        else:
            raise ValueError("Invalid wave type!")

    print("byte " + wave + "[] = {" + ", ".join(s) + "};")

if __name__ == '__main__':
    main()
