BOARD = arduino:avr:uno

# false
SYNTH_SERIAL ?= 1

# TODO: derive from arduino-cli board list
PORT ?= /dev/cu.usbmodemFD121

# generate wave values
SINE = $$( sys/wav.py sine )
SQUARE = $$( sys/wav.py sqre )

# define the wave constants
CFLAGS = --build-property "build.extra_flags=\"-DSINE_ARRAY=${SINE}\" \"-DSQUARE_ARRAY=${SQUARE}\" \"-DSYNTH_SERIAL=${SYNTH_SERIAL}\""

all: synth

target-install:
	arduino-cli core install arduino:avr

synth: synth.ino
	arduino-cli compile -b ${BOARD} ${CFLAGS} synth.ino

upload: synth
	arduino-cli upload -p ${PORT} -b ${BOARD} synth.ino

monitor: upload
	screen ${PORT}
