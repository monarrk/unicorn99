// write to PORTD (pins 0-7)
// values can be 0-255
// 0 = LOW
// 255 = HIGH
// 125 = 01111101, each bit corresponds to a pin
// voltage output from DAC = [ (value sent to PORTD) / 255 ] * 5V
#define pwrite(x) { PORTD = x; }

// pin to read the button from
#define BUTTON_PIN 13

// replaced with a byte array generated at compile time
// this is done for optimization (math takes a long time for arduino)
SINE_ARRAY;
SQUARE_ARRAY;

#define SIN 0
#define SQUARE 1

int mode = 0;
int prev_state;
int key_val;
int t = 0;

void setup() {
	// set digital pins 0-7 as outputs
	for (int i = 0; i < 8; i++) pinMode(i, OUTPUT);
	pinMode(BUTTON_PIN, INPUT);

	#if SYNTH_SERIAL
	// start serial
	Serial.begin(9600);
	#endif
	key_val = digitalRead(BUTTON_PIN);
}

void loop() {
	prev_state = key_val;
	key_val = digitalRead(BUTTON_PIN);

	// if button is pressed
	if (key_val == HIGH && prev_state == LOW) {
		#if SYNTH_SERIAL
		Serial.println(key_val);
		#endif
		
		// cycle through the wave modes
		mode++;
		if (mode > 1) mode = 0;
	};
	prev_state = key_val;

	if (mode == SIN) pwrite(sine[t])
	else if (mode == SQUARE) pwrite(sqre[t]);

	if (t < 100) t++; else t = 0;
}
