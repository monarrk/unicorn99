# UNI99
a drum machine

# building
you'll need
 - [arduino-cli](https://github.com/arduino/arduino-cli)
 - python3 (for wave generation at compile time)
 - an arduino uno (probably)
 - the synth build (TODO: publish)

you will also likely need to run `make target-install` to add the correct arduino target

you can run make with a couple different options:
 - `SERIAL_OUT=0` : enable serial output
 - `PORT={device_port}` : select the port to upload to

and with a couple different targets:
 - `make upload` : uploads the code to arduino
 - `make monitor` : uploads the code and monitors the serial connection
